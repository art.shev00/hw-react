

export const modal = {
    OPEN_MODAL: 'OPEN_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL'
}

export const openModalReducer = (state = {}, action) => {
    switch (action.type) {
        case modal.OPEN_MODAL:
            return { ...state, isOpenModal: true, productId: action.payload.product }
        case modal.CLOSE_MODAL:
            return { ...state, isOpenModal: false }
        default:
            return state
    }
}