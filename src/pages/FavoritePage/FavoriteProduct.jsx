import React from 'react'
import { useDispatch } from 'react-redux'
import { FaStar } from 'react-icons/fa';
import { favoriteActions } from '../../redux/favoriteReducer'
function FavoriteProduct({ product }) {

    const dispatch = useDispatch()

    return (
        <li className='cart__item'>
            <img className='cart__item-img' src={`${product.img_url}`} alt="" />
            <p className='cart__item-name'>{product.name}</p>

            <FaStar onClick={() => dispatch({ type: favoriteActions.REMOVE_FROM_FAVORITE, payload: { id: product.id } })} className='faStarInFav' />
        </li>

    )
}
export default FavoriteProduct