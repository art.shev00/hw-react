import { useState } from 'react'
import './quantity.scss'


function Quantity({ quantity, increment, decrement }) {
    return (
        <div className='quantity'>
            <p className='quantity-char' onClick={decrement}>-</p>
            <div className='quantity-num'>{quantity > 0 ? quantity : 0}</div>
            <p className='quantity-char' onClick={increment}>+</p>
        </div>

    )
}

export default Quantity