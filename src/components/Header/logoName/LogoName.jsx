import React from 'react';
import '../header.scss';

const ShopName = () => {
    let title = 'FapFapShop'
    return (
        <>
            <section>
                <h1>
                    {title.split('').map((letter, index) => {
                        return <span className='letter' key={index}>{letter}</span>
                    })}
                </h1>
            </section>
        </>

    )
}

export default ShopName