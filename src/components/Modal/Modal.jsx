import React from 'react';
import Button from '../Button/Button';
import './modal.scss'


export default function Modal({ closeModal, id, header, text, btn1, btn2 }) {

    return (
        <div className='modal'>
            <div id={id} className='overlay' onClick={closeModal}>
                <div onClick={(e) => {
                    e.stopPropagation()
                }} className='modal-container'>
                    <div className='modal-header'>
                        <button className='close-btn' onClick={closeModal}>Close</button>
                        <h1>{header}</h1>
                    </div>
                    <div className='modal-text'>
                        <p>{text}</p>
                    </div>
                    <div className='buttons'>
                        {btn1}
                        {btn2}
                    </div>
                </div>
            </div>
        </div>
    )
}
//}



// class Modal extends React.Component {
//     constructor(props) {
//         super(props)

//     }
//     render() {
//         let { open, closeModal } = this.props
//         if (!open) {
//             return null
//         } else {

//             return (
//                 <div className='modal'>
//                     <div id={this.props.id} className='overlay' onClick={closeModal}>
//                         <div onClick={(e) => {
//                             e.stopPropagation()
//                         }} className='modal-container'>
//                             <div className='modal-header'>
//                                 <button className='close-btn' onClick={closeModal}>Close</button>
//                                 <h1>{this.props.header}</h1>
//                             </div>
//                             <div className='modal-text'>
//                                 <p>{this.props.text}</p>
//                             </div>
//                             <div className='buttons'>
//                                 {this.props.btn1}
//                                 {this.props.btn2}
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             )
//         }

//     }
//}
// export default Modal;