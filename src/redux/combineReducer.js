import { combineReducers } from "redux";
import { productsReducer } from './productsReducer'
import { openModalReducer } from './openModalReducer'
import { favoriteReducer } from './favoriteReducer'
import { buscketReducer } from './buscketReducer'

export const rootReducer = combineReducers({
    products: productsReducer,
    modal: openModalReducer,
    saveFavorite: favoriteReducer,
    buscketReducer: buscketReducer
})

