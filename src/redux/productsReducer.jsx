
export const GET_PRODUCTS = 'GET_PRODUCTS'

export const productsReducer = (state = [], action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return { ...state, ...action.payload }
        default:
            return state
    }
}