import React, { useEffect, useState } from 'react';
import './busket.scss'
import './loader.scss'
import BusketProduct from './BusketProduct/BusketProduct'
import { useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux';
import FormBuy from '../../components/form/FormBuy';
import Hamsters from './Hamsters';

export default function CartPage() {

    const products = useSelector(state => state.products.products)
    const buscket = useSelector(state => state.buscketReducer)
    const navigate = useNavigate()

    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(buscket))
    }, [buscket])

    let inBusket = products !== undefined ? products.filter(product => buscket.includes(product.id)) : false
    return (
        <>
            {inBusket.length == 0 ?
                <Hamsters />

                :
                <div>
                    <div className='title'>
                        <p className='cart__title'>Make offer</p>
                        <button className="cart__back-button " onClick={() => navigate(-1)}> Return to product selection</button>
                    </div>


                    <div className='cart-page-buy'>
                        <div className='cart'>
                            <ul className='cart__list'>
                                {
                                    inBusket && inBusket.map(product => <BusketProduct product={product} key={product.id} />)
                                }
                            </ul>
                            <div className='total-price'></div>
                            <button className="cart__buy-button " > Buy</button>
                        </div>
                        <FormBuy />
                    </div>

                </div>
            }

        </>
    )


}
