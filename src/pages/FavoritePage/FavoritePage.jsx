import React from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import FavoriteProduct from './FavoriteProduct';

function FavoritePage() {
    const products = useSelector(state => state.products.products)
    const favorite = useSelector(state => state.saveFavorite)
    const inFavorite = products && products.filter(element => favorite.includes(element.id))
    const navigate = useNavigate()
    return (
        <div>
            <div className='title'>
                <p className='cart__title'>Your favorite products</p>
                <button className="cart__back-button " onClick={() => navigate(-1)}> Return to product selection</button>
            </div>

            <div className='cart'>
                <ul className='cart__list'>
                    {
                        inFavorite && inFavorite.map(prod => <FavoriteProduct key={prod.id} product={prod} />)
                    }
                </ul>
                <div className='total-price'></div>
                <button className="cart__buy-button " > Buy</button>
            </div>

        </div>
    )
}

export default FavoritePage