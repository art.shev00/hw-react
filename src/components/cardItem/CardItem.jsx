import React, { useEffect, useState } from 'react';
import { FaStar } from 'react-icons/fa';
import Button from '../Button/Button';
import { modal } from '../../redux/openModalReducer';
import { useDispatch, useSelector } from 'react-redux'
import './cardItemStyle.scss'

import { favoriteActions } from '../../redux/favoriteReducer'



export default function CardItem({ product }) {

    const [isFavoritAdd, setIsFavoritAdd] = useState(false);
    const [isYellowStar, setYellowStars] = useState(false)
    const favorite = useSelector(state => state.saveFavorite)

    let addToStar = (id) => {
        if (favorite.includes(id)) {
            console.log('working');
            return true
        } else {
            setIsFavoritAdd(!isFavoritAdd)
        }
    }


    const isProductInFav = (id) => {
        favorite.includes(id) ? true : false
    }

    const dispatch = useDispatch()

    return (
        <>
            <div className='card-item'>
                <p className='articul'>Код: {product.id}</p>
                <img src={`${product.img_url}`} alt="" />
                <p>{product.name}</p>
                <div className='flex star'>
                    <p onClick={() => {
                        dispatch({ type: favoriteActions.ADD_TO_FAVORITE, payload: { id: product.id } })
                        addToStar()

                    }} >
                        <FaStar className={(isFavoritAdd || isYellowStar ? "star-yellow" : '')} />  : Add to best</p>
                </div>

                <div className='flex'>
                    <p className='card-item-price'>{product.price}$</p>
                    <Button color="#24445e" text="Add to cart" onClick={() => dispatch({ type: modal.OPEN_MODAL, payload: { product: product.id } })} />
                </div>

            </div>


        </>
    )
}


