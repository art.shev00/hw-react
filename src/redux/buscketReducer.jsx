export const buscketActions = {
    ADD_TO_BUSCKET: 'ADD_TO_BUSCKET',
    REMOVE_FROM_BUSCKET: 'REMOVE_FROM_BUSCKET',
    CLEAN_BUSCKET: 'CLEAN_BUSCKET'
}
const saveBusket = JSON.parse(localStorage.getItem('products')) == null ? [] : JSON.parse(localStorage.getItem('products'));

export function buscketReducer(state = saveBusket, action) {
    switch (action.type) {
        case buscketActions.ADD_TO_BUSCKET:
            return (!state.includes(action.payload.id) ? [...state, action.payload.id] : [...state])

        case buscketActions.REMOVE_FROM_BUSCKET:
            return [...state.filter(element => element !== action.payload.id)]
        case buscketActions.CLEAN_BUSCKET:
            return []
        default:
            return state
    }
}