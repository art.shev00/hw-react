import React, { Component, useState } from 'react';
import { useSelector } from 'react-redux';

import CardItem from '../cardItem/CardItem';
import './cardListStyle.scss'

export default function CardList() {
    const products = useSelector(state => state.products.products)

    return (
        <div className='card-list'>
            {products && products.map(el => <CardItem
                product={el}
                key={el.name}
            />)}
        </div>
    )
}
