import { useState } from 'react'
import Quantity from '../quantity/Quantity'
import '../busket.scss'
import { BsFillBagXFill } from "react-icons/bs";
import { useDispatch } from 'react-redux'
import { buscketActions } from '../../../redux/buscketReducer'

function BusketProduct({ product }) {
    const [quantity, setQuantity] = useState(1)
    const dispatch = useDispatch()

    const increment = () => {
        if (quantity > 0 || quantity == 0) {
            setQuantity(quantity + 1)
        }
    }
    const decrement = () => {
        if (quantity > 0) {
            setQuantity(quantity - 1)
        }
    }

    return (
        <li className='cart__item'>
            <img className='cart__item-img' src={`${product.img_url}`} alt="" />
            <p className='cart__item-name'>{product.name}</p>
            <Quantity
                quantity={quantity}
                increment={() => increment()}
                decrement={() => decrement()}
            />
            <p className='cart__item-price'>{product.price * quantity}</p>
            <BsFillBagXFill
                onClick={() => dispatch({ type: buscketActions.REMOVE_FROM_BUSCKET, payload: { id: product.id } })}
                className='cart__item-delete'
            />
        </li>
    )
}

export default BusketProduct