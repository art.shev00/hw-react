import { createBrowserRouter } from "react-router-dom";

import React from "react";

import Main from './pages/Main/Main'
import BusketPage from './pages/Busket/BusketPage'
import CardList from "./components/cardList/CardList";
import FavoritePage from "./pages/FavoritePage/FavoritePage";


export const router = createBrowserRouter([
    {
        path: "/",
        element: <Main />,
        children: [
            {
                path: '/',
                element: <CardList />
            },
            {
                path: '/busket',
                element: <BusketPage />
            },
            {
                path: '/favorite',
                element: <FavoritePage />
            }
        ]
    },
]);