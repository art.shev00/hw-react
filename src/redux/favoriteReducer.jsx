export const favoriteActions = {
    ADD_TO_FAVORITE: 'ADD_TO_FAVORITE',
    REMOVE_FROM_FAVORITE: 'REMOVE_FROM_FAVORITE'
}
const saveFavorite = JSON.parse(localStorage.getItem('favorite')) == null ? [] : JSON.parse(localStorage.getItem('favorite'));

export function favoriteReducer(state = saveFavorite, action) {
    switch (action.type) {
        case favoriteActions.ADD_TO_FAVORITE:
            return state.includes(action.payload.id) ?
                [...state.filter(item => item != action.payload.id)]
                :
                [...state, action.payload.id]

        case favoriteActions.REMOVE_FROM_FAVORITE:
            return [...state.filter(element => element !== action.payload.id)]

        default:
            return state
    }
}

