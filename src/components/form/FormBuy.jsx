import React from 'react';
import { Formik } from 'formik';
import './form.scss'
import { validationSchema } from './validationBuscketSchema'
import { PatternFormat } from 'react-number-format';
import { buscketActions } from '../../redux/buscketReducer'
import { useDispatch } from 'react-redux';

function FormBuy() {
    const dispatch = useDispatch()
    return (
        <Formik
            initialValues={{ name: '', lastName: '', age: '', address: '', phone: '' }}
            onSubmit={(values, actions) => {
                setTimeout(() => {
                    console.log(JSON.stringify(values, null, 2));
                    localStorage.removeItem('products')
                    dispatch({ type: buscketActions.CLEAN_BUSCKET })
                    actions.setSubmitting(false);


                }, 1000);
            }}
            validationSchema={validationSchema}
        >

            {props => (
                <div className='form-container'>
                    <p>You have to fill fields behind</p>
                    <form className='form' onSubmit={props.handleSubmit}>
                        <label>Name</label>
                        <input
                            className='input'
                            type="text"
                            placeholder='Enter your name'
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={props.values.name}
                            name="name"
                        />

                        {props.touched.name && props.errors.name && <div id="feedback">{props.errors.name}</div>}
                        <label>Last Name</label>
                        <input
                            className='input'
                            type="text"
                            placeholder='Enter your lastname'
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={props.values.lastName}
                            name="lastName"
                        />
                        {props.touched.lastName && props.errors.lastName && <div id="feedback">{props.errors.lastName}</div>}
                        <label>Age</label>
                        <input
                            className='input'
                            type="number"
                            placeholder='Enter your age'
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={props.values.age}
                            name="age"
                        />
                        {props.touched.age && props.errors.age && <div id="feedback">{props.errors.age}</div>}
                        <label>Adress</label>
                        <input
                            className='input'
                            type="text"
                            placeholder='Enter your current adress'
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={props.values.address}
                            name="address"
                        />
                        {props.touched.address && props.errors.address && <div id="feedback">{props.errors.address}</div>}
                        <label>Phone</label>
                        <PatternFormat
                            className='input'
                            name='phone'
                            type='text'
                            value={props.values.phone}
                            format="(###) #### ###"
                            allowEmptyFormatting
                            mask="_"
                            onValueChange={(values) => {
                                props.setFieldValue('phone', values.value);
                            }}
                        />
                        {props.touched.phone && props.errors.phone ?
                            <div className='error'>{props.errors.phone}</div>
                            : null}
                        <button type="submit">Checkout</button>
                    </form>
                </div>
            )}


        </Formik>
    )
}

export default FormBuy