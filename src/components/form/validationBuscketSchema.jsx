import * as yup from 'yup'

const phoneRegExp = /^[0-9]{7,15}$/;

export const validationSchema = yup.object().shape({
    name: yup.string().max(20).required('Enter name'),
    lastName: yup.string().max(20).required('Enter lastname'),
    age: yup.number().integer().positive().required('Enter age'),
    address: yup.string().min(5).max(20).required('Enter address'),
    phone: yup
        .string()
        .required("Required"),
})
