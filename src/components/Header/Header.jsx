import ShopName from './logoName/LogoName';
import { FaCartArrowDown } from 'react-icons/fa';
import { FaStar } from 'react-icons/fa';
import './header.scss';

import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';


export default function Header() {

    const quantityBusket = () => buscket.length > 0 ? buscket.length : null
    const quantityFav = () => favorite.length > 0 ? favorite.length : null

    const favorite = useSelector(state => state.saveFavorite)
    const buscket = useSelector(state => state.buscketReducer)
    useEffect(() => {
        localStorage.setItem('favorite', JSON.stringify(favorite))
    }, [favorite])


    return (
        <>
            <div className='header'>
                <Link to="/">
                    <ShopName />
                </Link>

                <div className='flex flex-icon'>
                    <Link to="/busket">
                        <div className="shop-cart">
                            <FaCartArrowDown className='shop-cart' />
                            {buscket.length > 0 && <p className='count'>{quantityBusket()}</p>}

                        </div>
                    </Link>
                    <Link to="/favorite">
                        <div className='star-cart' >
                            <FaStar className='faStar' />
                            {favorite.length > 0 && <p className='count'>{quantityFav()}</p>}
                        </div>
                    </Link>

                </div>
            </div>
        </>
    )
}

