import { GET_PRODUCTS } from './productsReducer'

export function fetchProducts() {
    return (dispatch) => {
        fetch('./data/data.json')
            .then(response => response.json())
            .then(data => dispatch({ type: GET_PRODUCTS, payload: data }))
            .catch(error => console.error(error));
    }
}
