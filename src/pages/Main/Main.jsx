import React from 'react';
import Header from '../../components/Header/Header'
import { fetchProducts } from '../../redux/thunks'
import { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import Modal from '../../components/Modal/Modal'
import Button from '../../components/Button/Button'
//actions
import { modal } from '../../redux/openModalReducer'
import { buscketActions } from '../../redux/buscketReducer'

function Main() {
    const dispatch = useDispatch()

    const isModalOpen = useSelector(state => state.modal.isOpenModal)
    const productId = useSelector(state => state.modal.productId)

    useEffect(() => {
        dispatch(fetchProducts())
    }, [])


    return (
        <div>
            <Header />

            {isModalOpen && <Modal
                className='modal'
                closeModal={() => dispatch({ type: modal.CLOSE_MODAL })}
                header="Do you want to buy it ?" closeButton={true}
                text="If you press Buy, we add it to cart."

                btn1={
                    <Button onClick={() => {
                        dispatch({ type: buscketActions.ADD_TO_BUSCKET, payload: { id: productId } })
                        dispatch({ type: modal.CLOSE_MODAL })
                    }} text='Buy' color='#007580' />
                }
                btn2={<Button onClick={() => dispatch({ type: modal.CLOSE_MODAL })} text='Cancel' color='#4f4949' />}
            />}

            <Outlet />
        </div>
    )
}

export default Main